package demo.ErrorHandler;

public class CustomErrorHandler {
	
	public static void triggerError(String message) {
		System.err.println("Critical Error: "+message);
		System.exit(1);
	}

}

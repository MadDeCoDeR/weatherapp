package demo;

import javax.swing.JFrame;

import net.aksingh.owmjapis.api.APIException;
import net.aksingh.owmjapis.core.OWM;
import net.aksingh.owmjapis.model.CurrentWeather;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import demo.ErrorHandler.CustomErrorHandler;
import demo.IOManager.OutputManager;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import java.awt.event.ActionListener;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import javax.swing.JTextArea;
import java.awt.Color;
import java.awt.SystemColor;

public class mainApp {
	private static JTextField textField;
	private static OutputManager output;
	private static OWM owm;
	private static JFrame frame;
	private static JButton btnSearch;
	private static JLabel lblCityName;
	private static JTextArea textArea;

	public static void main(String[] args) {
		owm = new OWM("1bc595f73d94c0a78f5aedf47f0c6cb4");
		SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                initAndShowGUI();
            }
        });
		
	}
	
	private static void initAndShowGUI(){
		frame = new JFrame("Weather App");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		
		textArea = new JTextArea();
		textArea.setBackground(SystemColor.menu);
		textArea.setBounds(10, 64, 230, 135);
		frame.getContentPane().add(textArea);
		
		output = new OutputManager(textArea);
		
		textField = new JTextField();
		textField.setBounds(0, 33, 134, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		btnSearch = new JButton("Search");
		btnSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					CurrentWeather cwd = owm.currentWeatherByCityName(textField.getText());
					output.clear();
					 //printing city name from the retrieved data
			       output.print("City: " + cwd.getCityName());

			        // printing the max./min. temperature
			        output.print("Temperature: " + cwd.getMainData().getTempMax()
			                            + "/" + cwd.getMainData().getTempMin() + "\'K");
			        output.print("Wind: speed="+cwd.getWindData().getSpeed()
			        		+"/degree="+cwd.getWindData().getDegree());
				} catch (APIException e1) {
					// TODO Αυτόματα δημιουργημένη ενότητα catch
					CustomErrorHandler.triggerError(e1.getMessage());
				}
			}
		});
		btnSearch.setBounds(144, 32, 96, 23);
		frame.getContentPane().add(btnSearch);
		
		JLabel lblCityName = new JLabel("City Name:");
		lblCityName.setBounds(0, 8, 89, 14);
		frame.getContentPane().add(lblCityName);
		
		frame.getContentPane().setPreferredSize(new Dimension(240, 200));
		
		
        frame.pack();
        frame.setResizable(false);
        frame.setVisible(true);
	}
}

package demo.IOManager;

import javax.swing.JTextArea;

import demo.mainApp;

public class OutputManager {
	
	private JTextArea textarea;
	
	public OutputManager(JTextArea textarea) {
		this.textarea = textarea;
	}
	
	public void print(String message) {
		textarea.setText(textarea.getText() +"\n"+ message);
	}
	
	public void clear() {
		textarea.setText("");;
	}

}
